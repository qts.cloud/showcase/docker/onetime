from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.

class Encode(models.Model):
    text = models.TextField(max_length=2000, verbose_name="Write your secret below")
    expire = models.IntegerField(default=600, verbose_name="Expire (seconds)", validators=[MinValueValidator(60), MaxValueValidator(31536000)])

class Link(models.Model):
    url = models.URLField(max_length=100, verbose_name="Copy & Share this URL")
    token = models.TextField(max_length=100, verbose_name="Token ID", null=True)

class Decode(models.Model):
    token = models.CharField(max_length=100, verbose_name="Secret Token")

class Secret(models.Model):
    text = models.TextField(max_length=2000, verbose_name="Psst... Write it down. We have already forgotten it")
