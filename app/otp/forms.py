from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from .models import Encode, Link, Decode, Secret


class EncodeForm(forms.ModelForm):
    class Meta:
        model = Encode
        fields = ('text', 'expire')

    def __init__(self, *args, **kwargs):
        super(EncodeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Generate URL', css_class='btn btn-success'))


class LinkForm(forms.ModelForm):
    class Meta:
        model = Link
        fields = ('url',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Create new secret', css_class='btn btn-success'))

class DecodeForm(forms.ModelForm):
    class Meta:
        model = Decode
        fields = ('token',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Decode', css_class='btn btn-success'))

class SecretForm(forms.ModelForm):
    class Meta:
        model = Secret
        fields = ('text',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Generate a new one', css_class='btn btn-success'))
