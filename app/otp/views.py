from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, FormView
from django.urls import reverse_lazy, reverse

# Create your views here.
from .models import Encode, Link, Decode, Secret
from .forms import EncodeForm, LinkForm, DecodeForm, SecretForm
from .vault import wrap, unwrap

class EncodeView(FormView):
    model = Encode
    form_class = EncodeForm

    def form_valid(self, form):
        text = form.cleaned_data['text']
        ttl = form.cleaned_data['expire']
        token = wrap(text, ttl)
        self.kwargs['token'] = token['wrap_info']['token']
        response = super(EncodeView, self).form_valid(form)
        return response

    def get_success_url(self):
        token = self.kwargs['token']
        return reverse_lazy('otp:link', kwargs={'token': token})


class LinkView(FormView):
    model = Link
    form_class = LinkForm

    def get_initial(self):
        initial = super(LinkView, self).get_initial()
        _token_ = self.kwargs['token']
        _host_ = self.request.get_host()
        if self.request.is_secure():
            _proto_ = "https://"
        else:
            _proto_ = "http://"
        _url_ = f"{_proto_}{_host_}/decode/{_token_}"
        initial.update({'token': _token_, 'url': _url_})
        return initial

    def form_valid(self, form):
        response = super(LinkView, self).form_valid(form)
        return response

    def get_success_url(self):
        return reverse_lazy('otp:encode')


class DecodeView(FormView):
    model = Decode
    form_class = DecodeForm

    def get_initial(self):
        initial = super(DecodeView, self).get_initial()
        _token_ = self.kwargs.get('token', False)
        if _token_:
            initial.update({'token': _token_,})
        return initial

    def form_valid(self, form):
        self.kwargs['token'] = form.cleaned_data['token']
        response = super(DecodeView, self).form_valid(form)
        return response

    def get_success_url(self):
        token = self.kwargs['token']
        return reverse_lazy('otp:secret', kwargs={'token': token})

class SecretView(FormView):
    model = Secret
    form_class = SecretForm

    def get_initial(self):
        initial = super(SecretView, self).get_initial()
        _token_ = self.kwargs['token']
        _secret_ = unwrap(_token_)['data']['secret']

        initial.update({'text': _secret_,})
        return initial

    def form_valid(self, form):
        self.kwargs['text'] = form.cleaned_data['text']
        response = super(SecretView, self).form_valid(form)
        return response

    def get_success_url(self):
        return reverse_lazy('otp:encode')
