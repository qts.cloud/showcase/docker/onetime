#!/usr/bin/env python

import os
import hvac

vault_addr = os.environ.get('VAULT_ADDR', None)
vault_token = os.environ.get('VAULT_TOKEN', None)

client = hvac.Client(url=vault_addr)
client.token = vault_token


def wrap(secret, ttl):
    if ttl < 60:
        ttl = 60
    wrap_ttl = f"{int(ttl / 60)}m"
    try:
        result = client.write(
            path="sys/wrapping/wrap",
            wrap_ttl=wrap_ttl,
            secret=secret,
        )
    except hvac.exceptions.InvalidRequest as error:
        print(f"Error encoding secret: {error}")
        result = {"wrap_info": {"token": f"Error: {error}"}}
    return result

def unwrap(token):
    try:
        result = client.sys.unwrap(
            token=token,
        )
    except hvac.exceptions.InvalidRequest as error:
        print(f"Error decoding token `{token}`: {error}")
        result = {"data": {"secret": "Error: Token invalid/expired"}}
    return result
