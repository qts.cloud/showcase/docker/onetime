from django.urls import path
from . import views

app_name = 'otp'

urlpatterns = [
    path('', views.EncodeView.as_view(template_name='otp/encode.html'), name='encode'),
    path('encode/', views.EncodeView.as_view(template_name='otp/encode.html'), name='encode'),
    path('link/<str:token>', views.LinkView.as_view(template_name='otp/link.html'), name='link'),

    path('decode/', views.DecodeView.as_view(template_name='otp/decode.html'), name='decode'),
    path('decode/<str:token>', views.DecodeView.as_view(template_name='otp/decode.html'), name='confirm'),
    path('secret/<str:token>', views.SecretView.as_view(template_name='otp/display.html'), name='secret'),
]
