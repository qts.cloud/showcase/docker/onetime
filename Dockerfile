FROM python:alpine

WORKDIR /app

# Add files
ADD app .

# Install requirements
RUN apk update
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 80

CMD ["gunicorn", "-b", "0.0.0.0:80", "app.wsgi"]