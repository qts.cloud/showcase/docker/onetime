# **OneTimeSecrets** with **Docker** and Hashicorp's **Vault**

Description:
> Simple tool used to generate short-ttl  one-time secrets.  
  It uses Vault's wrap/unwrap tools.  
  Stateless!  
  Specifically designed for intra-net/self-hosting.
  Don't mind the lame interface or my non-existing Django skills :)

## Setup OpenVPN

* Prequisites:
  * basic linux knowledge
  * have docker already installed
  * have full access to the machine where docker is installed. We will refer to it as `## Setup OpenVPN

* Prequisites:
  * basic linux knowledge
  * have docker already installed
  * have full access to the machine where docker is installed. We will refer to it as `docker-vm` from now on.
  * be able to do port-forwarding to `docker-vm`
  * [vault container up and running](https://gitlab.com/qts.cloud/showcase/docker/vault)

* Setup:
  * connect to `docker-vm` machine and create your work directory

    ```sh
    ssh docker-vm
    # Preparing structure
    mkdir -p qts && cd qts
    git clone https://gitlab.com/qts.cloud/showcase/docker/onetime.git
    cd openvpn
    ```

  * Check out docker-compose.yml

    ```yaml
    version: "3"

    services:
      onetime:
        image: ibacalu/onetime:latest
        environment:
          VAULT_TOKEN: ${VAULT_TOKEN}
          VAULT_ADDR: ${VAULT_ADDR}
        restart: always
        ports:
          - 8080:80
    ```

  * Start Openvpn

    ```sh
    # Export Vault Config
    export VAULT_TOKEN='s.XXXXXXXXX'
    export VAUTL_ADDR='http://127.0.0.1:8200'

    # Launch container
    docker-compose up -d                # Run the container as a daemon
    docker-compose logs -f openvpn      # Follow the logs
    [...]
    onetime_1  | [INFO] Starting gunicorn 20.0.4
    onetime_1  | [INFO] Listening at: http://0.0.0.0:80 (1)
    onetime_1  | [INFO] Using worker: sync
    onetime_1  | [INFO] Booting worker with pid: 8
    [...]
    ```
